﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class GuiContainerElement : GuiElement
    {
        bool isInitialzed = false;
        public ElementCollection Elements { get; private set; }

        public GuiContainerElement()
        {
            Elements = new ElementCollection();
            Elements.Added += Elements_Added;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            isInitialzed = true;
            foreach (GuiElement ge in Elements)
            {
                ge.Initialize(Renderer);
            }
        }

        private void Elements_Added(object sender, ValueEventArgs<GuiElement> e)
        {
            if (isInitialzed)
            {
                e.Value.Initialize(Renderer);
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GuiElement element in Elements)
            {
                element.Update(gameTime);
            }
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            foreach (GuiElement element in Elements)
            {
                element.Draw(element.Renderer, gameTime);
            }
        }
    }

    public class ElementCollection : List<GuiElement>
    {
        public event EventHandler<ValueEventArgs<GuiElement>> Added;
        public ElementCollection()
            : base()
        {
        }

        public new void Add(GuiElement element)
        {
            if (Added != null)
            {
                Added(this, new ValueEventArgs<GuiElement>(element));
            }
            base.Add(element);
            
        }

        public new void AddRange(IEnumerable<GuiElement> collection)
        {
            if (Added != null)
            {
                foreach (GuiElement ge in collection)
                {
                    Added(this, new ValueEventArgs<GuiElement>(ge));
                }
            }
            base.AddRange(collection);
        }
    }

    public class ValueEventArgs<T> : EventArgs
    {
        public T Value { get; private set; }

        public ValueEventArgs(T value)
        {
            this.Value = value;
            
        }
    }
}
