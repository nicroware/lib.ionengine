﻿using NicroWare.Lib.IonEngine.Drawing;
using NicroWare.Lib.IonEngine.Font;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class ControlRenderer : SDLRenderer
    {
        SDLRenderer parrent;
        GuiElement container;

        public SDLPoint Location
        {
            get
            {
                return container.Location;
            }
        }

        public ControlRenderer(SDLRenderer renderer, GuiElement container)
            : base(renderer.BasePointer)
        {
            this.container = container;
            parrent = renderer;
        }

        private SDLRectangle ConvertRectangle(SDLRectangle rectangle)
        {
            return new SDLRectangle(rectangle.X + Location.X, rectangle.Y + Location.Y, rectangle.Width, rectangle.Height);
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle destination)
        {
            parrent.DrawTexture(texture, ConvertRectangle(destination));
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            parrent.DrawTexture(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip);
        }

        public override void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            parrent.DrawTexture(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip, color);
        }

        public override void DrawText(string text, SDLFont font, SDLRectangle destination, SDLColor color)
        {
            parrent.DrawText(text, font, ConvertRectangle(destination), color);
        }
    }
}
