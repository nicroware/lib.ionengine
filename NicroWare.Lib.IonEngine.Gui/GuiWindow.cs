﻿using NicroWare.Lib.IonEngine.Drawing;
using NicroWare.Lib.IonEngine.Font;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class GuiWindow : SDLRenderWindow
    {
        public PageScroller scroller;
        public GuiWindow()
        {

        }

        public void InitializeComponents(SDLRenderer renderer)
        {
            //string xmlPath = @"C:\Users\Nicolas\Desktop\CarGui.xml";
            string xmlName = this.GetType().Name + ".xml";
            XmlDocument document = new XmlDocument();
            document.Load(xmlName);
            ParseRoot(renderer, document.ChildNodes);
        }

        public void ParseRoot(SDLRenderer renderer, XmlNodeList list)
        {
            XmlNode window = null;// = list[0];
            foreach (XmlNode node in list)
            {
                if (node.Name == "Window")
                {
                    window = node;
                    break;
                }
            }
            if (window == null)
            {
                throw new Exception("Window node not found");
            }
            string[] winSize = (window.Attributes.GetNamedItem("Size")?.Value ?? "800;480").Split(';');
            string[] winLoc = (window.Attributes.GetNamedItem("Location")?.Value ?? "10;10").Split(';');
            string winTitle = window.Attributes.GetNamedItem("Title")?.Value ?? "Display1";
            this.Size = new SDLPoint(int.Parse(winSize[0]), int.Parse(winSize[1]));
            this.Location = new SDLPoint(int.Parse(winLoc[0]), int.Parse(winLoc[1]));
            this.Title = winTitle;
            foreach (XmlNode node in window.ChildNodes)
            {
                if (node.Name == "PageScroller")
                {
                    CreatePageScroller(renderer, node);
                }
            }
        }

        public void CreatePageScroller(SDLRenderer renderer, XmlNode node)
        {
            scroller = new PageScroller() { Location = new SDLPoint(0, 0), Size = Size };
            foreach (XmlNode chld in node.ChildNodes)
            {
                if (chld.Name == "Page")
                {
                    Panel page = new Panel();
                    foreach (XmlNode ctrl in chld.ChildNodes)
                    {
                        Assembly[] allAssms = AppDomain.CurrentDomain.GetAssemblies();
                        foreach (Assembly asm in allAssms)
                        {
                            Type[] t2 = asm.GetExportedTypes();
                            foreach (Type t in t2)
                            {
                                if (t.Name == ctrl.Name && typeof(GuiElement).IsAssignableFrom(t))
                                {
                                    object temp = Activator.CreateInstance(t);
                                    foreach (XmlAttribute attr in ctrl.Attributes)
                                    {
                                        if (attr.Name == "Name")
                                        {
                                            foreach (FieldInfo fi in GetType().GetRuntimeFields())
                                            {
                                                if (fi.Name == attr.Value)
                                                {
                                                    fi.SetValue(this, temp);
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PropertyInfo pi = t.GetRuntimeProperty(attr.Name);
                                            if (pi != null)
                                            {
                                                if (pi.PropertyType == typeof(SDLPoint))
                                                {
                                                    string[] point = attr.Value.Split(';');
                                                    pi.SetValue(temp, new SDLPoint(int.Parse(point[0]), int.Parse(point[1])));
                                                }
                                                else if (pi.PropertyType == typeof(int))
                                                {
                                                    pi.SetValue(temp, int.Parse(attr.Value));
                                                }
                                                else if (pi.PropertyType == typeof(bool))
                                                {
                                                    pi.SetValue(temp, bool.Parse(attr.Value));
                                                }
                                                else if (pi.PropertyType.IsEnum)
                                                {
                                                    pi.SetValue(temp, Enum.Parse(pi.PropertyType, attr.Value));
                                                }
                                                else if (pi.PropertyType == typeof(string))
                                                {
                                                    pi.SetValue(temp, attr.Value.Replace("\\n", "\n"));
                                                }
                                                else if (pi.PropertyType == typeof(SDLFont))
                                                {
                                                    foreach (FieldInfo fInfo in GetType().GetRuntimeFields())
                                                    {
                                                        if (fInfo.Name == attr.Value)
                                                        {
                                                            pi.SetValue(temp, fInfo.GetValue(this));
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                EventInfo ei = t.GetRuntimeEvent(attr.Name);
                                                MethodInfo[] all = GetType().GetRuntimeMethods().ToArray();
                                                MethodInfo mi = null;
                                                foreach (MethodInfo minfo in all)
                                                {
                                                    if (minfo.Name == attr.Value)
                                                    {
                                                        mi = minfo;
                                                        break;
                                                    }
                                                }
                                                if (ei != null && mi != null)
                                                {
                                                    ei.AddEventHandler(temp, mi.CreateDelegate(ei.EventHandlerType, this));
                                                }
                                            }
                                        }
                                    }
                                    page.Elements.Add(temp as GuiElement);
                                    break;
                                }
                            }
                        }
                    }
                    page.Initialize(renderer);
                    scroller.Elements.Add(page);
                }
            }
        }
    }


}
