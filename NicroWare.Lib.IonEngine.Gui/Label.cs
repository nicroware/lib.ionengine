﻿using System;
using NicroWare.Lib.IonEngine.Drawing;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class Label : GuiTextElement
    {

        /*SDLSurface preText;
        SDLTexture text;*/

        public override int FontSize
        {
            get
            {
                return base.FontSize;
            }
            set
            {
                base.FontSize = value;
                base.Size = new SDLPoint(base.Size.X, value);
            }
        }

        public override SDLPoint Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                base.FontSize = value.Y;
            }
        }

        public Label()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(100, 40);
            Text = "Label";
            DynamicTextSize = true;

        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            //text = SDLTexture.CreateFrom(renderer, preText);
            //renderer.Draw(text, new SDLRectangle(Location.X, Location.Y, /*(int)(Size.Y / 1.5 * Text.Length)*/ Size.X, Size.Y));
            if (!DynamicTextSize)
            {
                renderer.DrawText(Text, Font, new SDLRectangle(0, 0, Size.X, Size.Y), TextColor);
            }
            else
            {
                int startX = CalculateAlign(Text);
                int width = (int)(Size.Y * 0.6 * Text.Length);
                renderer.DrawText(Text, Font, new SDLRectangle(startX, 0, width, FontSize), TextColor);
            }
        }
    }
}

