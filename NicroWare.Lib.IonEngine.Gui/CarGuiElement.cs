﻿using System;
using NicroWare.Lib.IonEngine.Drawing;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class CarGuiElement : GuiValueElement
    {
        public SDLColor FrameColor { get; set; }
        public SDLColor ValueColor { get; set; }
        public CarGuiElement()
        {
            FrameColor = SDLColor.Red;
            ValueColor = SDLColor.Red;
        }
    }
}

