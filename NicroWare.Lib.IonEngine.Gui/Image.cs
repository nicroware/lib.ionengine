﻿using System;
using NicroWare.Lib.IonEngine.Drawing;
using System.IO;

namespace NicroWare.Lib.IonEngine.Gui
{
    public class Image : GuiElement
    {
        public SDLTexture ImageTexture { get; set; }
        public SDLColor ImageColor { get; set; }
        string source;
        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
                if (renderer != null)
                    ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(source));
            }
        }

        SDLRenderer renderer;

        public Image()
        {
            Size = new SDLPoint(0, 0);
            ImageColor = SDLColor.White;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            this.renderer = renderer;
            if (source != null)
                ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(source));
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            //TODO: Do some work on the image class
            if (Size.X == 0 && Size.Y == 0)
            {
                renderer.DrawTexture(ImageTexture, new SDLRectangle(0, 0, ImageTexture.Width, ImageTexture.Height), new SDLRectangle(0,0,ImageTexture.Width, ImageTexture.Height), 0, new SDLPoint(0,0), RendererFlip.None, ImageColor);
            }
            else
                renderer.DrawTexture(ImageTexture, new SDLRectangle(0, 0, ImageTexture.Width, ImageTexture.Height), new SDLRectangle(0,0,Size.X, Size.Y), 0, new SDLPoint(0,0), RendererFlip.None, ImageColor);
        }

        public static Image LoadFromFile(SDLRenderer renderer, string path)
        {
            return LoadFromFile(renderer,new FileInfo( path));
        }

        public static Image LoadFromFile(SDLRenderer renderer, FileInfo info)
        {
            if (!info.Exists)
                throw new FileNotFoundException();
            else if (info.Extension.ToUpper() != ".BMP")
                throw new FileLoadException("Wrong file format");
            return new Image() { ImageTexture = SDLTexture.CreateFrom(renderer, SDLSurface.LoadBitmap(info.FullName)) };
        }
    }
}

