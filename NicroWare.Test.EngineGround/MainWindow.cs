﻿using NicroWare.Lib.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.IonEngine;
using System.IO;
using NicroWare.Lib.IonEngine.Font;

namespace NicroWare.Test.EngineGround
{
    public class MainWindow : GuiWindow
    {
        SDLRenderer renderer;
        public SDLFont digiFont;

        public MainWindow()
        {
            renderer = SDLRenderer.Create(this);
            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");

            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 50);
            InitializeComponents(renderer);
            Global.SideScroll = true;
            ShowCursor = true;
        }

        public override void Update(GameTime gameTime)
        {
            scroller.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();
            scroller.Draw(renderer, gameTime);
            renderer.Present();
        }
    }
}
