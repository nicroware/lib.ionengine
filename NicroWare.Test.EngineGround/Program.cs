﻿using NicroWare.Lib.IonEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Test.EngineGround
{
    class Program
    {
        static void Main(string[] args)
        {
            MainWindow window = new MainWindow();
            window.Run();
        }
    }
}
